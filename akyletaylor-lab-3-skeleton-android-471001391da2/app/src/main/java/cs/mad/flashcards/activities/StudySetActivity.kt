package cs.mad.flashcards.activities

import android.content.DialogInterface
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import cs.mad.flashcards.databinding.ActivityStudySetBinding
import cs.mad.flashcards.entities.Flashcard
import cs.mad.flashcards.entities.getHardcodedFlashcards

class StudySetActivity : AppCompatActivity() {
    private lateinit var binding: ActivityStudySetBinding
    private val flashcards = getHardcodedFlashcards().toMutableList()
    private val missedCards = mutableListOf<Flashcard>()
    private val initialCount = flashcards.size
    private var completedCount = 0
    private val missedCount
        get() = missedCards.size
    private var correctCount = 0
    private var isCardFlipped = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityStudySetBinding.inflate(layoutInflater)
        setContentView(binding.root)
        updateCard()

        binding.masteredText.text = "$completedCount / $initialCount"
        binding.backButton.setOnClickListener { finish() }
        binding.missedButton.setOnClickListener { missCurrent() }
        binding.skipButton.setOnClickListener { skipCurrent() }
        binding.correctButton.setOnClickListener { markCorrectCurrent() }

        binding.cardView.setOnClickListener {
            flipCard()
        }
    }

    private fun missCurrent() {
        val current = flashcards.removeFirst()
        missedCards.add(current)
        flashcards.add(current)
        binding.missedCountText.text = "Missed: $missedCount"
        isCardFlipped = false
        updateCard()
    }

    private fun skipCurrent() {
        val current = flashcards.removeFirst()
        flashcards.add(current)
        isCardFlipped = false
        updateCard()
    }

    private fun markCorrectCurrent() {
        val current = flashcards.removeFirst()
        if (flashcards.size == 0) {
            finish()
        } else {
            completedCount += 1
            binding.masteredText.text = "$completedCount / $initialCount"

            if (!missedCards.contains(current)) {
                correctCount += 1
                binding.correctCountText.text = "Correct: $correctCount"
            }

            isCardFlipped = false
            updateCard()
        }
    }

    private fun flipCard() {
        isCardFlipped = !isCardFlipped
        updateCard()
    }

    private fun updateCard() {
        if (isCardFlipped) {
            binding.flashcardTerm.text = flashcards[0].answer
        } else {
            binding.flashcardTerm.text = flashcards[0].question
        }
    }

    fun alert() {
        AlertDialog.Builder(this)
            .setTitle("My title")
            .setMessage("My message")
            .setPositiveButton("EZ") { dialogInterface: DialogInterface, i: Int ->
                // do something on click
            }
            .create()
            .show()
    }
}