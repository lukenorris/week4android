package cs.mad.flashcards.entities


data class Flashcard(
    val question: String,
    val answer: String
)

fun getHardcodedFlashcards(): List<Flashcard> {
    return listOf(Flashcard("one", "eins"),
        Flashcard( "two", "zwei"),
        Flashcard( "three", "drei"),
        Flashcard( "four", "vier"),
        Flashcard( "five", "funf"),
        Flashcard( "six", "sechs"),
        Flashcard( "seven", "sieben"),
        Flashcard( "eight", "acht"),
        Flashcard( "nine", "neun"),
        Flashcard( "ten", "zehn"),
    )
}